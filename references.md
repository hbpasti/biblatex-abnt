# References



ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS. *NBR 10520*: Informação e documentação: citações em documentos: apresentação. Rio de Janeiro, 2002.

USP. Sistema Integrado de Bibliotecas da USP; FUNARO, Vânia Martins Bueno de Oliveira et al. *Diretrizes para apresentação de dissertações e teses da USP*: documento eletrônico e impresso. Parte I (ABNT). 2. ed. rev. ampl. São Paulo: Sistema Integrado de Bibliotecas da USP, 2009. 102 p. (Cadernos de Estudos; 9) Disponível em: <http://www.usp.br/sibi/produtos/imgs/Caderno_Estudos_9_PT_1.pdf>
